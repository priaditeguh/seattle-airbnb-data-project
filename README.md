# Seattle AirBNB Data Project

### Summary ###

This project use a public dataset from [Seattle AirBNB](https://www.kaggle.com/airbnb/seattle/data). We analyze the data by following CRISP-DM (Cross Industry Process for Data Mining) process. 
The process consist of several steps :

* **Bussiness Understanding**

We investigate the data to answer some business question as follows :
1. In which time interval the home rental price reaches the highest peak during the year 2016?
2. What words that are most relevant to good or bad rating towards a rental homestay?
3. How well can we predict a rental price? What aspects correlate well to the rental price?

* **Data Understanding**

We get some insight from the Seattle AirBNB survey data to answer these questions.

* **Prepare Data**

Before analyzing the data, we must wrangle the data first. The wrangling is handled per case depending on the question and the data. It could be that we need to impute missing values or remove them. We also make the categorical variables compatible with the machine learning model. 


* **Model Data**

After the data is handled properly, we can model the data. But only questions 2 and 3 use a model to help to get insight from the data. Question 2 use linear regression and question 3 use ridge regression. For question 1, we only use descriptive statistics. For question 2, we need to do an additional feature engineering to extract features from text data before feeding it into a machine learning model.


* **Results**

Results are the findings from our wrangling and modeling. These are the answers we find to each of the questions.


* **Deploy**

We deploy by presenting our findings on a [medium blog post](https://medium.com/@priaditeguhw/crisp-dm-brief-explanation-and-example-f0d8bc923ed5). In the blog post, we communicate our findings to a non-technical audience.
 

### Library ###
1. pandas 1.0.5
2. numpy 1.18.5
3. matplotlib 3.2.2
4. sklearn 0.22.2.post1
5. re 2.2.1
6. nltk 3.2.5
7. wordcloud 1.5.0

### File Structure ###
```
- data
| - calendar.csv  # listing id and the price and availability for that day  
| - listings.csv  # homestay full descriptions and average review score
| - reviews.csv # unique id for each reviewer and detailed comments
- Seattle AirBNB.ipynb # jupyter notebook contains the code to analyze the data  
- README.md
```

### Results ###

* **Question 1**

From the plot below, we can see that the peak interval is above 150. So we can check which date has price larger than 150.

After filtering the data, we find that the rental price reached peak season from late May to early September. This finding makes sense since that period is summer and people tend to spend their vacation during this time, especially for 4 seasons countries like the USA. 

![Average price for each date during 2016](image/price_2016.png)


* **Question 2**

From the wordcloud visualization below, we can grasp the idea of what makes the customer happy. Mainly, these are the hospitality of the host and its homestay quality. Other things that make a good review are walking distance with important places (such as tourist attraction or public transportation) or having an amazing view. 

![Phrases of positive reviews](image/positive_review.png)

We capture one review that contains 'walking distance'.
```
The listing was exactly as described!  Kelly's place was wonderful and cleen.  it was just what we were looking for.
We only got to meet her for a moment one evening because of our varied schedule, but she was warm and welcoming to us.
Her place is also in good walking distance to City Center!  Lots to do right around there!
We would recommend her place to others who are traveling to Seattle!
```

![Phrases of negative reviews](image/negative_review.PNG)

Although there are some ambiguous words, we can still get some insights about bad reviews. Some of the main reasons for bad reviews are unsatisfactory rented room and unfriendly host ('fgghjdxc' might be a censored word expressing disappointment). We can also interpret some phrases as the service supposedly existing, like hand soap. We suspect that the customer is disappointed because the hand soap is not provided or maybe it is not appropriate to be served. We find one example below. 

We capture one review that contains 'hand soap'.
```
Awesome views, great location within walking distance of all the best Seattle has to offer. 
Private parking space was a plus and the condo was tidy. 
The only downside was there was no hand soap or toilet paper... 
Luckily there was a grocery store next door! Lastly, Tony was accommodating when we showed up early.
```

* **Question 3**

Top 10 Variables             |  Bottom 10 Variables
:-------------------------:|:-------------------------:
![](image/top10_coefficients.png)  |  ![](image/bottom10_coefficients.PNG)


Interpreting regression coefficients for numerical and categorical variables are different. For numerical variables, a unit increase results in an increase/decrease in average rental price by coefficient units, if all other variables held constant. And for a categorical variable, the average rental price is higher/lower by coefficient units for 1 value than 0 value, if all other variables held constant.

From the left table of coefficients above, we see the top 10 variables which contribute to an increase in the average rental price. Meanwhile, in the right table of coefficients above, we see the bottom 10 variables which contribute to a decrease in the average rental price, as their coefficients are negative.

The table shows a rational explanation for the correlation between the facilities and the rental price. For example, a boat must be very expensive to rent and a property with more bedroom or bathroom lead to a more expensive price. Meanwhile, the shared room is affordable accommodation where travelers usually rent and a dorm is affordable for students.

Likewise, our model is able to capture the correlation between location and rental price quite well. If check a property price website, [Zillow](https://thewww.zillow.com/), the price comparison is similar.

1. The median home value in whole [Seattle](https://www.zillow.com/seattle-wa/home-values/) is $767,906.

2. The median home value in [Magnolia](https://www.zillow.com/magnolia-seattle-wa/home-values/) neighbourhood is $1,027,236.

3. The median home value in [Northgate](https://www.zillow.com/northgate-seattle-wa/home-values/)  neighbourhood is $469,294.

And both table shows that Magnolia neighborhood has a coefficient of 21.58 and the Northgate neighborhood has a coefficient of -25.45. It means that the rental price in Magnolia is higher than average and the rental price in Northgate is lower.

### Acknowledgement ###
1. [Udacity](https://www.udacity.com/)
2. [AirBNB](http://insideairbnb.com/get-the-data.html)